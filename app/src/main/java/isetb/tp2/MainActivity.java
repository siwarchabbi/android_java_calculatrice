package isetb.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText c1, c2;
    TextView resultat, txt_calculation;
    String operation = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c1 = findViewById(R.id.c1);
        c2 = findViewById(R.id.c2);
        resultat = findViewById(R.id.txt_res);
        txt_calculation = findViewById(R.id.txt_calculation);
    }

    public void chooseOperation(View view) {
        operation = view.getTag().toString();
        txt_calculation.setText("Opération choisie : " + operation);
    }

    public void calculate(View view) {
        String text1 = c1.getText().toString();
        String text2 = c2.getText().toString();

        if (!text1.isEmpty() && !text2.isEmpty()) {
            int chiffre1 = Integer.valueOf(text1);
            int chiffre2 = Integer.valueOf(text2);
            int s = 0;

            switch (operation) {
                case "+":
                    s = chiffre1 + chiffre2;
                    break;
                case "-":
                    if (chiffre1 >= chiffre2) {
                        s = chiffre1 - chiffre2;
                    } else {
                        Toast.makeText(this, "Le premier nombre doit être supérieur ou égal au deuxième pour la soustraction", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    break;
                case "*":
                    s = chiffre1 * chiffre2;
                    break;
                case "/":
                    if (chiffre2 != 0) {
                        s = chiffre1 / chiffre2;
                    } else {
                        Toast.makeText(this, "Division par zéro impossible", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    break;
            }

            String calculation = text1 + " " + operation + " " + text2 + " = " + s;
            txt_calculation.setText(calculation);
            resultat.setText(String.valueOf(s));
        } else {
            Toast.makeText(this, "Veuillez entrer les deux chiffres", Toast.LENGTH_SHORT).show();
        }
    }
}
